## Simple Pytest Unit Test

Having a python file that starts with the word 'test' makes it so you can execute the pytest and it will run whatever functions 
start with the word 'test' in your script.

In order to generate junit xml, execute the command like this.

	pytest --junitxml results.xml

This will run the tests and put the output into the results file. Junit xml is a standard format which can be consumed by a 
variety of test running tools, including gitlab testrunners.
